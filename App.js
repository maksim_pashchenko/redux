// import React, {Component} from 'react';
// import {Provider} from 'react-redux';
// import {createStore} from 'redux';
// import {reducer} from './src/reducers/index'
//
// const store = createStore(reducer);
//
// import Counter from './src/components/Counter'
//
// class App extends Component {
//     render() {
//         return (
//             <Provider store={store}>
//                 <Counter/>
//             </Provider>
//         )
//     }
// }
//
// export default App


import React, {Component} from 'react';
import {Provider} from 'react-redux';
import {createStore} from 'redux';
import {reducer} from './src/reducers/index'

const store = createStore(reducer);

import AddList from './src/containers/AddList';
import ActionList from './src/containers/ActionList';
import List from './src/components/List';
import {View, StyleSheet} from "react-native";

class App extends Component {
    render() {
        return (
            <Provider store={store}>
                <View style={styles.container}>
                    <AddList/>

                    <View style={{height: 200}}>
                        <List/>
                    </View>

                    <ActionList/>

                </View>
            </Provider>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

});

export default App

