export const INCREMENT = 'INCREMENT';
export const DECREMENT = 'DECREMENT';


export const ADD_LIST = 'ADD_LIST';
export const TOGGLE_LIST = 'TOGGLE_LIST';
export const REMOVE_LIST = 'REMOVE_LIST';
