import {INCREMENT, DECREMENT, ADD_LIST, TOGGLE_LIST, REMOVE_LIST} from '../actions/actionTypes';

const initialState = {
    value: 0,

    inputValue: '',
    list: [],
};

export function reducer(state = initialState, action) {
    switch (action.type) {
        case INCREMENT:
            return Object.assign({}, state, {
                value: state.value + 1,
            });
        case DECREMENT:
            return Object.assign({}, state, {
                value: state.value - 1,
            });
        case ADD_LIST:
            return {
                ...state,
                list: [...state.list, action.data],
            };
        case TOGGLE_LIST: {
            const list = state.list.map(item => {
                return (item.id === action.id) ? {...item, completed: !item.completed} : item;
            });

            return {
                ...state, list,
            };

        }
        case REMOVE_LIST:
            let list = state.list.filter((item) => {
                return !item.completed;
            });

            return {
                ...state, list,
            };

        default:
            return state;
    }
}

export const mapStateToProps = state => {
    return {
        value: state.value,
        list: state.list,
    };
};

export const mapDispatchToProps = dispatch => {
    return {
        increment: () => dispatch(incValue()),
        decrement: () => dispatch(decValue()),
        addList: (item) => dispatch(addList(item)),
        toggleList: (id) => dispatch(toggleList(id)),
        removeList: (id) => dispatch(removeList(id)),
    };
};

function incValue() {
    return {
        type: INCREMENT,
    };
}

function decValue() {
    return {
        type: DECREMENT,
    };
}

let id = 0;

function addList(item) {
    return {
        type: ADD_LIST,
        data: {
            id: id++,
            title: item,
            completed: false,
        },
    };
}

function toggleList(id) {
    return {
        type: TOGGLE_LIST,
        id: id,
    };
}

function removeList(id) {
    return {
        type: REMOVE_LIST,
        id,
    };
}
