import React, {Component} from 'react';
import {View, Text, Button} from 'react-native';
import { connect } from 'react-redux';

import {mapStateToProps, mapDispatchToProps} from '../reducers/index'

class Counter extends Component {
    // constructor(props) {
    //     super(props)
    // }

    render() {
        return(
            <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <Text>{this.props.value}</Text>

                <Button
                    title="Increment"
                    onPress={() => this.props.increment()}/>

                <Button
                    title="Decrement"
                    onPress={() => this.props.decrement()}/>
            </View>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Counter)
