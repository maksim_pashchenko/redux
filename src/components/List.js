import React, {Component} from 'react';
import {View, FlatList, Text, TouchableOpacity, StyleSheet} from 'react-native';

import {CheckBox} from 'react-native-elements';

import {mapStateToProps, mapDispatchToProps} from '../reducers/index';

import {connect} from 'react-redux';

class List extends Component {
    render() {
        return (
            <View>
                <FlatList
                    data={this.props.list}
                    renderItem={({item}) => {
                        return (
                            <TouchableOpacity
                                style={styles.container}
                                onPress={() => {
                                    this.props.toggleList(item.id);
                                }}>

                                <CheckBox
                                    checked={item.completed}
                                    onPress={() => {
                                        this.props.toggleList(item.id);
                                    }} />

                                <Text
                                    // style={styles.text}
                                    style={Object.assign({textDecorationLine: item.completed ? 'line-through' : ''}, styles.text)}
                                >{item.title}</Text>
                            </TouchableOpacity>
                        );
                    }}/>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        alignItems: 'center',
        // marginVertical: 10,
    },

    checkbox: {
        width: 20,
        height: 20,
        marginRight: 0,
        padding: 0
    },

    text: {
        fontSize: 20,
    },
});

export default connect(mapStateToProps, mapDispatchToProps)(List);


