import React, {Component} from 'react';
import {View, Button, StyleSheet} from 'react-native';

import {mapDispatchToProps, mapStateToProps} from '../reducers';
import {connect} from 'react-redux';


class ActionList extends Component {
    constructor(props) {
        super(props);
    }

    // state = {
    //     completedItem: [],
    // };

    render() {
        return (
            <View>
                <Button
                    title="Remove"
                    onPress={() => {
                        const list = this.props.list;

                        const completedItem = [];

                        list.map((key) => {
                            key.completed ? completedItem.push(key.id) : '';
                            // key.completed ? this.setState({
                            //     completedItem: [...this.state.completedItem, key.id]
                            // }) : '';
                        });

                        this.props.removeList(completedItem);
                    }}/>
            </View>
        );
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ActionList);
