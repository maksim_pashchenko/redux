import React, {Component} from 'react';
import {View, Text, TextInput, Button, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {mapDispatchToProps, mapStateToProps} from "../reducers";

class AddList extends Component {
    constructor(props) {
        super(props)
    }

    state = {
        value: ''
    };

    render() {
        return (
            <View style={styles.container}>
                <TextInput
                    placeholder='Enter item'
                    value={this.state.value}
                    onChangeText={(text) => this.setState({value: text})}
                    style={styles.input}
                />
                <Button
                    title="Add"
                    onPress={() => {
                        if (this.state.value.length > 0) {
                            this.props.addList(this.state.value);
                            this.setState({value: ''});
                        }
                        // console.log(this.props.list)
                    }}/>
            </View>
        )
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(AddList)


const styles = StyleSheet.create({
    container: {
        // marginTop: 500,
        flexDirection: 'row'
        // flex: 1,
        // justifyContent: 'center',
        // alignItems: 'center'
    },

    input: {
        width: 150,
        padding: 8,
        backgroundColor: '#f5f5f5'
    }
});
